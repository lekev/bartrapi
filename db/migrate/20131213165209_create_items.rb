class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.references :user
      t.references :category
      t.string :name
      t.integer :price

      t.timestamps
    end
    add_index :items, :user_id
    add_index :items, :category_id
  end
end
