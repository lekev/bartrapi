class AddCreditToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions, :credit, :integer
  end
end
