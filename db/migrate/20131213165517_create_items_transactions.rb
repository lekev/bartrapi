class CreateItemsTransactions < ActiveRecord::Migration
  def change
    create_table :items_transactions do |t|
      t.references :item
      t.references :transaction

      t.timestamps
    end
    add_index :items_transactions, :item_id
    add_index :items_transactions, :transaction_id
  end
end
