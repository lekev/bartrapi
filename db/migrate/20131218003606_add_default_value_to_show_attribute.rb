class AddDefaultValueToShowAttribute < ActiveRecord::Migration
  def up
    change_column :transactions, :credit, :integer, :default => 0
end

def down
    change_column :transactions, :credit, :integer, :default => 0
end
end
