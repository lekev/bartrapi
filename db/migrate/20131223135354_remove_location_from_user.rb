class RemoveLocationFromUser < ActiveRecord::Migration
  def up
    remove_column :users, :location_lat
    remove_column :users, :location_long
  end

  def down
    add_column :users, :location_long, :string
    add_column :users, :location_lat, :string
  end
end
