class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :user
      t.references :item
      t.string :status

      t.timestamps
    end
    add_index :transactions, :user_id
    add_index :transactions, :item_id
  end
end
