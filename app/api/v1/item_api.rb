module V1
	class ItemApi < Base
		format :json

			# CORS for swaggers
	before do
    header['Access-Control-Allow-Origin'] = '*'
    header['Access-Control-Request-Method'] = '*'
	end
	guard_all!
		resource :items do

			desc "Return all Item around a point"
				params do
					optional :longitude ,type: Float, desc: "Position longitude for the request"
					optional :latitude ,type: Float , desc: "Position latitude for the request"
					optional :distance, type: Float, desc: "Max distance from the position in KM"

				end
				get do

					if params[:longitude] && params[:latitude] && params[:distance]
						@items = Item.where("user_id != ?",current_user).within(params[:distance], :origin => [params[:latitude],params[:longitude]] )
					else
						@items = Item.where("user_id != ?",current_user)
					end

					present @items , with: V1::Entities::Item
				end

		end
	end
end
