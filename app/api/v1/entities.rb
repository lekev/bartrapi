module V1
  module Entities 
    
    class User < Grape::Entity
      expose :id
      expose :email
      expose :first_name
      expose :last_name
      expose :phone
      expose :longitude
      expose :latitude
       expose :avatar do |user, options|
        user.avatar.url(:medium)
      end
    end

    class Item < Grape::Entity
      expose :id, :name
      expose :category_id
      expose :price
      expose :latitude
      expose :longitude
      expose :status
      expose :image1 do |status, options|
        status.image1.url(:medium)
      end
       expose :image2 do |status, options|
         status.image2.url(:medium)
      end
       expose :image3 do |status, options|
        status.image3.url(:medium)
      end
      expose :image4 do |status, options|
        status.image4.url(:medium)
      end

      expose :user do |status, options|
        {id: status.user.id, first_name: status.user.first_name, last_name: status.user.last_name, picture: status.user.avatar.url(:medium) }
      end

      expose :created_at
       
    end

    class Transaction < Grape::Entity
     expose :id, :status, :credit
     
      expose :category_id
      expose :price

    
      expose :item, using: V1::Entities::Item

      expose :user do |transaction, options|
        {id: transaction.user.id, first_name: transaction.user.first_name, last_name: transaction.user.last_name }
      end

      expose :items_propositions, using: V1::Entities::Item 

    end
    
  end
end