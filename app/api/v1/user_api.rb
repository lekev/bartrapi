module V1
	class UserApi < Base
		format :json

			# CORS for swaggers
	before do
    header['Access-Control-Allow-Origin'] = '*'
    header['Access-Control-Request-Method'] = '*'
	end
	guard_all!
		namespace :users do


			desc "Create a User"
			params do
				requires :email, type: String, desc: "User Email "
				requires :password, type: String, desc: "User Password "
				requires :password_confirmation , type: String, desc: "User Password password_confirmation"
				requires :first_name, type: String, desc: "User firstname"
				requires :last_name, type: String, desc: "User lastname "
				requires :phone_number, type: String,desc: "User Phone number"
				requires :location_long, type: String, desc: "User Home long"
				requires :location_lat, type: String, desc: "User Home lat"
				optional :avatar , type: String , desc: "User Avatar"
			end
			post do
				@user = User.new
				@user.email = params[:email] if params[:email]
				@user.password = params[:password] if params[:password]
				@user.password_confirmation = params[:password_confirmation] if params[:password_confirmation]
				@user.first_name = params[:first_name] if params[:first_name]
				@user.last_name = params[:last_name] if params[:last_name]
				@user.phone = params[:last_name] if params[:last_name]
				@user.location_long = params[:location_long] if params[:location_long]
				@user.location_lat = params[:location_lat] if params[:location_lat]
				@user.avatar = params[:avatar] if params[:avatar]

				if @user.save
						present @user , with: V1::Entities::User
					else
						error! @user.errors
					end
			end

			route_param :user_id do

				desc 'Return user informations'
				get do 
					present User.find(params[:user_id]) , with: V1::Entities::User
				end

				desc 'Return all user Items'
				get 'items' do
					present User.find(params[:user_id]).items , with: V1::Entities::Item
				end
			end

		end
	end
end
