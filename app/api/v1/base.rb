module V1
class Base < BartrApi
		
 version "v1", :using => :header, :vendor => 'bartr'


  mount V1::Credential
  mount V1::UserApi
  mount V1::ItemApi

end
end


# HASHIE PAPER CLIP ADAPTER
module Paperclip
  class HashieMashUploadedFileAdapter < AbstractAdapter

    def initialize(target)
      @tempfile, @content_type, @size = target.tempfile, target.type, target.tempfile.size
      self.original_filename = target.filename
    end

  end
end

Paperclip.io_adapters.register Paperclip::HashieMashUploadedFileAdapter do |target|
  target.is_a? Hashie::Mash
end