

module V1
	class Credential < Base

		format :json


			# CORS for swaggers
	before do
    header['Access-Control-Allow-Origin'] = '*'
    header['Access-Control-Request-Method'] = '*'
	end

		guard_all!

		namespace :me do

			desc "Return User Base Information"
			get  do
				present current_user , with: V1::Entities::User
				
			end

			desc "Edit the current user"
			params do
				optional :email, type: String, desc: "User Email "
				optional :first_name, type: String, desc: "User firstname"
				optional :last_name, type: String, desc: "User lastname "
				optional :phone_number, type: String,desc: "User Phone number"
				optional :longitude, type: Float, desc: "Item category id "
				optional :latitude, type: Float, desc: "Item category id "
				optional :avatar  , desc: "User Avatar"

			end
			put do
				@user= current_user
				@user.email = params[:email] if params[:email]
				@user.first_name = params[:first_name] if params[:first_name]
				@user.last_name = params[:last_name] if params[:last_name]
				@user.phone = params[:phone_number] if params[:phone_number]
				@user.longitude = params[:longitude] if params[:longitude]
				@user.latitude = params[:latitude] if params[:latitude]
				@user.avatar =  params[:avatar] if params[:avatar]

				if @user.save
						present @user , with: V1::Entities::User
					else
						error! @user.errors
				end
			end


			resource :items do

				desc "Return all personal Items"
				get do
					present current_user.items , with: V1::Entities::Item
				end

				## => ##########
				## => ITEM #####
				## => ##########

				desc "Create a Item"
				params do
					requires :name, type: String, desc: "Item name or description"
					requires :category_id, type: Integer, desc: "Item category id "
					optional :price , type: Integer
					optional :latitude , type: Float
					optional :longitude , type: Float
					optional :image1
					optional :image2
					optional :image3
					optional :image4


				end
				post do


					@item= Item.new
					@item.name = params[:name] if params[:name]
					@item.category_id = params[:category_id] if params[:category_id]
					@item.price = params[:price] if params[:price]
					@item.latitude = params[:latitude] if params[:latitude]
					@item.longitude = params[:longitude] if params[:longitude]	
					@item.user_id = current_user.id
					
					@item.image1 =  params[:image1] if params[:image1]
					@item.image2 =  params[:image2]	if params[:image2]
					@item.image3 =  params[:image3] if params[:image3]
					@item.image4 =  params[:image4] if params[:image4]
                


					if @item.save
						present @item , with: V1::Entities::Item
					else
						error! @item.errors
					end

				end

				route_param :item_id do

					desc "Edit an item"
					params do
						optional :name, type: String, desc: "Item name or description"
						optional :category_id, type: Integer, desc: "Item category id "
						optional :latitude , type: Float
						optional :price , type: Integer
						optional :longitude , type: Float

					optional :image1
					optional :image2
					optional :image3
					optional :image4
					end
					put do
						@item = current_user.items.find( params[:item_id])
						@item.name = params[:name] if params[:name]
						@item.category_id = params[:category_id] if params[:category_id]
						@item.price = params[:price] if params[:price]
						@item.latitude = params[:latitude] if params[:latitude]
						@item.longitude = params[:longitude] if params[:longitude]	

						@item.image1 =  params[:image1] if params[:image1]
						@item.image2 =  params[:image2]	if params[:image2]
						@item.image3 =  params[:image3] if params[:image3]
						@item.image4 =  params[:image4] if params[:image4]


						if @item.save
							present @item, with: V1::Entities::Item
						else
							error! @item.errors
						end
					end

					desc "destroy user item"
					delete do
						@item = current_user.items.find(params[:item_id])
						@item.destroy
					end

					desc "proposition of transactions for the item"
					get '/popositions' do
						@item = current_user.items.find(params[:item_id])
						@item.propositions
					end

				end



			end

			###
			### => Transactions
			###

			resource :propositions do

				desc "Return all User's item propositions"
				get do
					present current_user.propositions,  with: V1::Entities::Transaction
				end

				route_param :poposition_id do

					desc "answer to a proposition"
					params do
						requires :status ,type: String,values: ["accepted", "rejected"], desc: "Proposition status [accepted, rejected]"
					end
					post '/answer' do
						@proposition = current_user.propositions.find(params[:poposition_id])
						@proposition.status =  params[:status]

						if params[:status] == 'accepted'

							#Change status for the item change and all the object from the propositions
							@proposition.item.status = "bartered"

							 @proposition.items_propositions.each do |item|
							 	item.status = "bartered"
							 	item.save
							 end

							 #  make the transaction for the credit
							 if @proposition.credit
							 @proposition.user.credit = @proposition.user.credit - @proposition.credit
							 @proposition.item.user.credit = @proposition.item.user.credit + @proposition.credit
							end

							 # Free all the item attech propositions
							 @proposition.item.propositions.where(status: 'attending').each do |proposition|
							 	proposition.status = 'rejected'
							 	
								error! proposition.errors unless proposition.save
							 end
							 

						end
						if @proposition.save
							present @proposition, with: V1::Entities::Transaction
						else
							error! @proposition.errors
						end
						
					end

				end

			end


			resource :offers do

				route_param :trans_id do
				desc "Return specific user transaction"
				get do

						@offer  = current_user.offers.find(params[:trans_id])

					present @offer , with: V1::Entities::Transaction
				end
			end



			 	desc "Return all user transactions"

			 	get do 
			 		@offer = current_user.offers
					present @offer , with: V1::Entities::Transaction
			 	end

				desc "add new offers"
				params do
					requires :item_id ,type: Integer
					requires :items_transactions ,type: Array
					optional :credit , type: Integer, default: 0
				end
				post do
					@offer = Transaction.new
					@offer.user_id = current_user.id
					@offer.credit = params[:credit]
					@offer.item_id = params[:item_id]


					error! @offer.errors unless @offer.save

					if params[:items_transactions]
						params[:items_transactions].each do |i|
							@items_transaction = ItemsTransactions.new
							@items_transaction.item_id = i
							@items_transaction.transaction_id = @offer.id
							@items_transaction.save
						end
					end
					present @offer , with: V1::Entities::Transaction
				end
			end


		end
	end
end

