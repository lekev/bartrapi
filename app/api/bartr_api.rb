
class BartrApi < Grape::API

	include APIGuard


	format :json

	mount V1::Base

	rescue_from ActiveRecord::RecordNotFound do |e|
		message = e.message.gsub(/\s*\[.*\Z/, '')
		Rack::Response.new(
		[{ status: 404, status_code: "not_found", error: message }.to_json],
		404,
		{ 'Content-Type' => 'application/json' }
		)
	end

	rescue_from ActiveRecord::RecordInvalid do |e|
		record = e.record
		message = e.message.downcase.capitalize
		Rack::Response.new(
		[{ status: 403, status_code: "record_invalid", error: message }.to_json],
		403,
		{ 'Content-Type' => 'application/json' }
		)
	end



	add_swagger_documentation mount_path: '/doc',
	api_version: 'v1',
	markdown: true,
	hide_documentation_path: true,
	base_path: Bartr::Application.config.base_path+"/api"
	



end

