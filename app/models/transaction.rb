class Transaction < ActiveRecord::Base



  attr_accessible :status, :credit, :user_id, :item_id, :items_proposition_ids

  default_scope includes :user, :item

  belongs_to :user
  belongs_to :item

has_and_belongs_to_many :items_propositions ,class_name: "Item"

  


  validates :status, inclusion: { in: %w(pending accepted rejected),
  message: "%{value} is not a valid statut  ['pending','accepted', 'rejected']" }

  after_initialize do
    if new_record?
      self.status ||= 'pending' # be VERY careful with ||= and False values
    end
  end


  def status_enum
    # Do not select any value, or add any blank field. RailsAdmin will do it for you.
    ['pending','accepted', 'rejected']
  end

  


  #  rails_admin do
  #   edit do
  #     field :items_transaction do
  #       orderable true
  #     end
  #   end
  # end

end
