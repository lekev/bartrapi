class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


   acts_as_mappable :default_units => :kms,
  :lat_column_name => :latitude,
  :lng_column_name => :longitude


  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me ,:first_name, :last_name, :phone, :latitude, :longitude,
  :credit, :items_attributes, :item_ids, :avatar
  
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  
  has_many :propositions, through: :items
  has_many :offers , class_name: "Transaction"
  accepts_nested_attributes_for :offers, :allow_destroy => true

  has_many :items,  dependent: :destroy
  accepts_nested_attributes_for :items, :allow_destroy => true

 def self.authenticate!(email, password)
    user = User.where(email: email).first
    return (user.valid_password?(password) ? user : nil) unless user.nil?
    nil
  end


   


end
