class Item < ActiveRecord::Base


  belongs_to :user
  belongs_to :category

  has_many :propositions , class_name: "Transaction"

  has_and_belongs_to_many :transactions

  has_attached_file :image1, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  has_attached_file :image2, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  has_attached_file :image3, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  has_attached_file :image4, :styles => { :medium => "300x300>", :thumb => "100x100>" }


  acts_as_mappable :default_units => :kms,
  :lat_column_name => :latitude,
  :lng_column_name => :longitude

 attr_accessible :name, :price, :category_id, :user_id, :status, :latitude, :longitude,
  :image1, :image2, :image3, :image4

  validates :name, presence: true

  validates :status, inclusion: { in: %w(visible attending bartered),
  message: "%{value} is not a valid statut ['attending', 'visible', 'bartered']" }



  after_initialize do
    if new_record?
      self.status ||= 'attending' # be VERY careful with ||= and False values
    end
  end

 def status_enum
    # Do not select any value, or add any blank field. RailsAdmin will do it for you.
    ['attending', 'visible', 'bartered']
  end

  #asking from active_admin
  attr_accessor :delete_asset
  before_validation { self.asset.clear if self.delete_asset == '1' }

  


end



